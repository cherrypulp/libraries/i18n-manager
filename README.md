# I18N Manager

[![Build Status](https://travis-ci.org/blok/i18n-manager.svg?branch=master)](https://travis-ci.org/blok/i18n-manager)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/blok/i18n-manager/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/blok/i18n-manager/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/blok/i18n-manager/badge.svg?branch=master)](https://coveralls.io/github/blok/i18n-manager?branch=master)

[![Packagist](https://img.shields.io/packagist/v/blok/i18n-manager.svg)](https://packagist.org/packages/blok/i18n-manager)
[![Packagist](https://poser.pugx.org/blok/i18n-manager/d/total.svg)](https://packagist.org/packages/blok/i18n-manager)
[![Packagist](https://img.shields.io/packagist/l/blok/i18n-manager.svg)](https://packagist.org/packages/blok/i18n-manager)

Package description:

Import a Google spreadsheet into your Laravel Application

## Installation

Install via composer

```bash
composer require blok/i18n-manager
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Blok\I18nManager\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section

```php
Blok\I18nManager\Facades\I18nManager::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Blok\I18nManager\ServiceProvider" --tag="config"
```

## Usage

Your google spreadsheet have to have this structure :

| REF | EN | FR | NL | created_at | updated_at |
|-----|----|----|----|------------|------------|
|     |    |    |    |            |            |

Then you will need to publish the spreadsheet as a CSV output by clicking on File > Publish to the web

https://www.evernote.com/l/AHdbGjzj0jFLNKuZTvxsAVGK_6dnpDKCvGg

After that you need to add the url of the csv file into config/i18n-manager.php config files.

Then depending of your business logic, you can call the :

- Blok\I18nManager\Jobs\TranslationsExporterJob : to export your existing laravel translations (/!\ will simply output an html table that you will need to copy-paste into your Google Spreadsheets)
- Blok\I18nManager\Jobs\TranslationsImporterJob : to import translations back into your Laravel application

You can also call the import/export like that :

```php

app('i18n-manager')->export();

app('i18n-manager')->import();

```

# Exemples of integration

For exemple in an AdminController, if in a protected route user add labels_export=1 or labels_import=1 => it will export or import labels.

```php
class AdminController{
    public function index(){
      if(request()->has('labels_export')){
          app('i18n-manager')->export();
      }

      if(request()->has('labels_import')){
          app('i18n-manager')->import();
      }
    }
}
```

## How to blacklist some non exported labels/references ?

Sometimes you don't want to update/sync some labels into google spreadsheets. To blacklist some references, you can add it into the config/i18n-manager.php and adding your own regex.

## How to allow the creation of new ref by the customer ?

Warning!!! : This is something that might used only in a testing environment as it might cause unexpected behavior on production if the user override by mistake an other ref !!

After exporting the config file in the config/i18n-manager.php set :

```php
<?php

return [
    //...
    'allow_new_ref' => true,
    //...
];
```

You can also create or override on the fly the config right after the import for instance :

```php
class AdminController{
    public function index(){
      if(request()->has('labels_export')){
          app('i18n-manager')->export();
      }

      if(request()->has('add_new_ref')){
        config(['i18n-manager' => true]);
      }

      if(request()->has('labels_import')){
          app('i18n-manager')->import();
      }
    }
}
```

## Warning

This package doesn't give a way to automatically create a spreadsheet + update the spreadsheet as it requires more cumbersome autorization from Google API.

## Security

If you discover any security related issues, please email
instead of using the issue tracker.

## Credits

- [Cherry Pulp](https://github.com/cherrypulp)
