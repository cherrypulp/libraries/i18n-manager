<?php

namespace Blok\I18nManager\Tests;

use Blok\I18nManager\Facades\I18nManager;
use Blok\I18nManager\ServiceProvider;
use Orchestra\Testbench\TestCase;

class I18nManagerTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'i18n-manager' => I18nManager::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
