<?php

namespace Blok\I18nManager\Jobs;

use Blok\Utils\Arr;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TranslationsImporterJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $spreadsheetUrl = config('i18n-manager.spreadsheet_url');

        $data = Arr::csvToArray($spreadsheetUrl, ['skipFirstRow' => true, 'indexFromFirstRow' => true, 'delimiter' => ',']);

        /**
         * Define some blacklisted ref (project specific)
         */
        $blacklist = config('i18n-manager.blacklist', []);
        $allowNewRef = config('i18n-manager.allow_new_ref');

        foreach ($data as $row) {

            /**
             * if the ref is blacklisted => it's skipped
             */
            if ($blacklist && preg_match('/' . implode('|', $blacklist) . '/i', $row['REF'])) {
                continue;
            }

            /**
             * For each ref found in the spreadsheet we build the correct file in php or json
             */
            foreach (config('app.locales') as $lang) {

                $path = app()->langPath() . '/'.$lang;

                if (preg_match('/\//i', $row['REF'])) {
                    $folders = explode('/', $row['REF']);
                    $folder = $path . DIRECTORY_SEPARATOR . $folders[0];
                    if (is_dir($folder)) {
                        $key = str_replace($folders[0] . '/', '', $row['REF']);
                        $files = explode('.', $key);
                        $filename = $folder . DIRECTORY_SEPARATOR . $files[0] . '.php';
                        if (is_file($filename)) {
                            $data = include($filename);

                            if ($data) {
                                $datadot = array_dot($data);
                                $key = substr($key, strlen($files[0] . DIRECTORY_SEPARATOR));
                                if (isset($datadot[$key]) || $allowNewRef) {
                                    $datadot[$key] = $row[strtoupper($lang)];
                                    $dataarray = Arr::dot_array($datadot);
                                    file_put_contents($filename, "<?php \n" . "\n"
                                        . "return " . self::varexport($dataarray, true) . ";");
                                    continue;
                                }
                            }
                        }
                    }
                }

                $key = $row['REF'];
                $folder = $path;
                $files = explode('.', $key);
                $filename = $folder . DIRECTORY_SEPARATOR . $files[0] . '.php';

                if (is_file($filename)) {
                    $data = include($filename);

                    if ($data) {
                        $datadot = array_dot($data);
                        $key = substr($key, strlen($files[0] . DIRECTORY_SEPARATOR));
                        if (isset($datadot[$key]) || $allowNewRef) {
                            $datadot[$key] = $row[strtoupper($lang)];
                            $dataarray = Arr::dot_array($datadot);
                            file_put_contents($filename, "<?php \n" . "\n"
                                . "return " . self::varexport($dataarray, true) . ";");
                            continue;
                        }
                    }
                } else {
                    $jsonPath = app()->langPath() . DIRECTORY_SEPARATOR . $lang.'.json';
                    $jsonLang = file_get_contents($jsonPath);

                    if ($jsonLang) {
                        $data = json_decode($jsonLang, true);

                        if (isset($data[$row['REF']]) || $allowNewRef) {
                            $data[$row['REF']] = $row[strtoupper($lang)];
                        }

                        file_put_contents($jsonPath, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                        continue;
                    }
                }
            }
        }
    }

    /**
     * Make a beautiful var_export with indentation and short notation
     *
     * @param $expression
     * @param false $return
     * @return string
     */
    public static function varexport($expression, $return = FALSE)
    {
        $export = var_export($expression, TRUE);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [NULL, ']$1', ' => ['], $array);
        $export = implode(PHP_EOL, array_filter(["["] + $array));

        if ((bool)$return) {
            return $export;
        }

        echo $export;
    }
}
