<?php

namespace Blok\I18nManager\Jobs;

use Blok\I18nManager\I18nManager;
use Blok\Utils\Arr;
use Blok\Utils\Utils;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class TranslationsExporterJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = I18nManager::getLabels();

        $data = array_dot($data);

        $translatedKeys = Arr::csvToArray(config('i18n-manager.spreadsheet_url'), ['skipFirstRow' => true, 'indexFromFirstRow' => true, 'delimiter' => ',']);

        $translatedKeys = collect($translatedKeys)->pluck('REF')->toArray();

        $all = request()->get('all', false);

        $blacklist = config('i18n-manager.blacklist', []);

        echo "<table>";

        foreach ($data as $key => $value) {
            // if key in the blacklist => we skip it
            if ($blacklist && preg_match('/' . implode('|', $blacklist) . '/i', $key)) {
                continue;
            }

            try {
                //
                if (!is_array($value) && !is_array($key)) {
                    if (!in_array($key, $translatedKeys) && !$all) {
                        echo "<tr>";

                        echo "<td>" . htmlentities($key) . "</td>";

                        foreach (config('i18n-manager.locales', config('app.locales')) as $locale) {
                            $translation = __($key, [], $locale);

                            if ($locale === $key) {
                                $translation = '';
                            }

                            echo "<td>".htmlentities($translation)."</td>";
                        }

                        echo "<td>" . date('d/m/Y H:i:s') . "</td></tr>";

                    }
                }
            } catch (\Throwable $e) {
                if (config('app.debug')) {
                    throw $e;
                }
            }
        }
        echo "</table>";
    }
}
