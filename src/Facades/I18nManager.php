<?php

namespace Blok\I18nManager\Facades;

use Illuminate\Support\Facades\Facade;

class I18nManager extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'i18n-manager';
    }
}
