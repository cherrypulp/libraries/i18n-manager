<?php

namespace Blok\I18nManager;

use Blok\I18nManager\Jobs\TranslationsExporterJob;
use Blok\I18nManager\Jobs\TranslationsImporterJob;

class I18nManager
{
    /**
     * Call the export jobs
     */
    public function export()
    {
        (new TranslationsExporterJob())->handle();
    }

    /**
     * Call the import job
     */
    public function import()
    {
        (new TranslationsImporterJob())->handle();
    }

    /**
     * Return all the labels
     */
    public static function getLabels()
    {
        $lang = config('app.locale');

        if (request()->has('lang')) {
            $lang = request()->get('lang');
        }

        $files = glob(resource_path('lang/' . $lang . '/*.php'));
        $json = resource_path('lang/' . $lang . '.json');
        $strings = [];

        if (file_exists($json)) {
            $json = file_get_contents($json);
            $strings = json_decode($json, true);
            $strings = \Blok\Utils\Arr::dot_array($strings);
        }

        foreach ($files as $file) {
            $name = basename($file, '.php');
            $strings[$name] = require $file;
        }

        $files = glob(resource_path('lang/' . $lang . '/**/*.php'));

        foreach ($files as $file) {
            $path = str_replace(base_path('resources/lang/'), '', $file);
            $part = explode('/', $path);
            unset($part[0]);
            array_pop($part);
            $name = basename($file, '.php');
            $strings[implode('/', $part) . '/' . $name] = require $file;
        }

        return $strings;
    }
}
