<?php

namespace Blok\I18nManager;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/i18n-manager.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('i18n-manager.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'i18n-manager'
        );

        $this->app->bind('i18n-manager', function () {
            return new I18nManager();
        });
    }
}
